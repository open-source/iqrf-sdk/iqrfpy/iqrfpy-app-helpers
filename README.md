## iqrfpy-app-helpers

An extension for [iqrfpy](https://pypi.org/project/iqrfpy/) offering basic application logic examples.

## Quick start

Before installing the library, it is recommended to first create a virtual environment.
Virtual environments help isolate python installations as well as pip packages independent of the operating system.

A virtual environment can be created and launched using the following commands:

```bash
python3 -m venv <dir>
source <dir>/bin/activate
```

iqrfpy-app-helpers can be installed using the pip utility:

```bash
python3 -m pip install -U iqrfpy-app-helpers
```
