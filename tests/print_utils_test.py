from datetime import datetime, timezone
from io import StringIO
import unittest
from unittest.mock import patch

from parameterized import parameterized

from app_helpers import PrintUtils, TransportType


class PrintUtilsTestCase(unittest.TestCase):

    @parameterized.expand([
        [
            'Timestamp: ',
            datetime.fromtimestamp(1713774740, tz=timezone.utc),
            'Timestamp:             22.04.2024  08:32:20.000\n'
        ]
    ])
    def test_print_timestamp_pair(self, text: str, dt: datetime, expected):
        with patch('sys.stdout', new_callable=StringIO) as fake_stdout:
            PrintUtils.print_timestamp_pair(text=text, dt=dt, stream=fake_stdout)
            self.assertEqual(
                fake_stdout.getvalue(),
                expected
            )

    @parameterized.expand([
        ['One:', 1, 'One:                   1\n'],
        ['Two:', '2', 'Two:                   2\n'],
        ['Three:', 'Three', 'Three:                 Three\n'],
        ['Four:', 4.0, 'Four:                  4.0\n'],
        ['Transport:', TransportType.IQRF_IDE, 'Transport:             IQRF IDE\n'],
    ])
    def test_print_pair(self, text: str, data, expected: str):
        with patch('sys.stdout', new_callable=StringIO) as fake_stdout:
            PrintUtils.print_pair(text, data, stream=fake_stdout)
            self.assertEqual(
                fake_stdout.getvalue(),
                expected
            )

    @parameterized.expand([
        ['One:', 1, 5, 'One:  1\n'],
        ['Two:', '2', 10, 'Two:       2\n'],
        ['Transport:', TransportType.IQRF_IDE, 0, 'Transport: IQRF IDE\n'],
    ])
    def test_print_pair_spaces(self, text: str, data, spaces: int, expected: str):
        with patch('sys.stdout', new_callable=StringIO) as fake_stdout:
            PrintUtils.print_pair(text, data, spaces=spaces, stream=fake_stdout)
            self.assertEqual(
                fake_stdout.getvalue(),
                expected
            )

    @parameterized.expand([
        [
            'test', '=', False, False,
            '\n'.join(['test', '====\n'])
        ],
        [
            'test', '=', True, False,
            '\n'.join(['====', 'test', '====\n'])
        ],
        [
            'test', '=', False, True,
            '\n'.join(['\ntest', '====\n'])
        ],
        [
            'test', '=', True, True,
            '\n'.join(['\n====', 'test', '====\n'])
        ]
    ])
    def test_print_underlined(self, text: str, underline_char: str, upper_line: bool, line_break: bool, expected: str):
        with patch('sys.stdout', new_callable=StringIO) as fake_stdout:
            PrintUtils.print_underlined(text=text, char=underline_char, upper_line=upper_line, line_break=line_break)
            self.assertEqual(
                fake_stdout.getvalue(),
                expected
            )


if __name__ == '__main__':
    unittest.main()
