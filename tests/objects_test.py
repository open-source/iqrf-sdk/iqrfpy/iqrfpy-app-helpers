import unittest

from parameterized import parameterized
from app_helpers.objects import TransportType


class TransportTypeTestCase(unittest.TestCase):

    @parameterized.expand([
        [TransportType.MQTT, 'MQTT'],
        [TransportType.IQRF_IDE, 'IQRF IDE'],
    ])
    def test_to_str(self, val: TransportType, expected: str):
        self.assertEqual(
            str(val),
            expected
        )


if __name__ == '__main__':
    unittest.main()
