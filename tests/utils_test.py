from typing import List
import unittest
from parameterized import parameterized

from app_helpers import convert_bytes_to_std_sensor
from iqrfpy.utils.sensor_constants import SensorTypes
from iqrfpy.utils.quantity_data import Temperature
from iqrfpy.utils.sensor_parser import SensorData


class UtilsTestCase(unittest.TestCase):

    @parameterized.expand([
        [
            SensorTypes.TEMPERATURE,
            [0x01, 0x02],
            SensorData(
                sensor_type=Temperature.type,
                name=Temperature.name,
                short_name=Temperature.short_name,
                unit=Temperature.unit,
                frc_commands=Temperature.frc_commands,
                decimal_places=Temperature.decimal_places,
                index=0,
                value=32.0625,
            ),
        ]
    ])
    def test_convert_bytes_to_std_sensor(self, sensor_type: SensorTypes, data: List[int], expected: SensorData):
        self.assertEqual(
            convert_bytes_to_std_sensor(sensor_type, data),
            expected
        )


if __name__ == '__main__':
    unittest.main()
