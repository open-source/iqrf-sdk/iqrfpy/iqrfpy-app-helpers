from datetime import datetime, timezone
import unittest

from app_helpers import StringFormatter, TransportType
from parameterized import parameterized


class StringFormatterTestCase(unittest.TestCase):

    @parameterized.expand([
        [datetime.fromtimestamp(1713774740, tz=timezone.utc), '22.04.2024  08:32:20.000'],
        [datetime.utcfromtimestamp(1713774740).replace(microsecond=532 * 1000), '22.04.2024  08:32:20.532'],
    ])
    def test_format_time(self, dt: datetime, expected: str):
        self.assertEqual(
            StringFormatter.format_time(dt),
            expected
        )

    @parameterized.expand([
        ['One:', 1, 'One:                   1'],
        ['Two:', '2', 'Two:                   2'],
        ['Three:', 'Three', 'Three:                 Three'],
        ['Four:', 4.0, 'Four:                  4.0'],
        ['Transport:', TransportType.IQRF_IDE, 'Transport:             IQRF IDE'],
    ])
    def test_format_pair_default(self, text: str, val, expected: str):
        self.assertEqual(
            StringFormatter.format_pair(text, val),
            expected
        )


if __name__ == '__main__':
    unittest.main()
