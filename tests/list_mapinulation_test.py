import unittest
from typing import List

from app_helpers import ListManipulation
from parameterized import parameterized


class ListManipulationTestCase(unittest.TestCase):

    @parameterized.expand([
        [[1], '1'],
        [[1, 1], '1, 1'],
        [[1, 3, 5], '1, 3, 5'],
        [[1, 2, 4], '<1,2>, 4'],
        [[1, 2, 4, 6, 7], '<1,2>, 4, <6,7>'],
        [[1, 3, 4, 5], '1, <3,5>'],
        [[1, 2, 4, 5, 7], '<1,2>, <4,5>, 7'],
        [[1, 2, 4, 5, 7, 8], '<1,2>, <4,5>, <7,8>'],
        [[1, 3, 4, 6], '1, <3,4>, 6']
    ])
    def test_get_intervals_from_list(self, data: List[int], expected: str):
        self.assertEqual(
            ListManipulation.get_intervals_from_list(data),
            expected,
        )

    @parameterized.expand([
        [[1], '1'],
        [[1, 3, 5], '1, 3, 5'],
        [[1, 2, 4], '<1,2>, 4'],
        [[1, 2, 4, 6, 7], '<1,2>, 4, <6,7>'],
        [[1, 3, 4, 5], '1, <3,5>'],
        [[1, 2, 4, 5, 7], '<1,2>, <4,5>, 7'],
        [[1, 2, 4, 5, 7, 8], '<1,2>, <4,5>, <7,8>'],
        [[1, 3, 4, 6], '1, <3,4>, 6']
    ])
    def test_get_list_from_intervals(self, expected: List[int], data: str):
        self.assertEqual(
            ListManipulation.get_list_from_intervals(data),
            expected
        )


if __name__ == '__main__':
    unittest.main()
