## Changelog

### Version: 0.1.0

Release date: 30.04.2024

#### Changes

- Added methods to enable and disable IQRF IDE logging
- Added `log_level` parameter to `IqrfApplication` constructor
- Added `set_logging_level` method to `IqrfApplication`
- Fixed package description

### Version: 0.1.0.dev2

Release date: 23.04.2024

#### Changes

- Added `tabulate` dependency
- Format `NtwDevice` device identification and TR configuration strings to table

### Version: 0.1.0.dev1

Release date: 22.04.2024

#### Changes

- Initial implementation
