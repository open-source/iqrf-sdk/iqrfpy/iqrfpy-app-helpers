.PHONY: docs

build:
	python3 -m pip install --upgrade build
	python3 -m build

docs:
	rm -rf docs
	pdoc -d google -o docs app_helpers/

install:
	python3 -m pip install .

upload-test:
	python3 -m pip install --upgrade twine
	python3 -m twine upload --repository testpypi dist/*

upload:
	python3 -m pip install --upgrade twine
	python3 -m twine upload dist/*

clean:
	rm -rf build dist iqrfpy_app_helpers.egg-info .pytest_cache htmlcov docs
	find . -type d -name  "__pycache__" -exec rm -r {} +

codestyle:
	pycodestyle app_helpers

docstyle:
	pydocstyle app_helpers

lint:
	pylint app_helpers

pytest:
	python3 -m pytest -v tests

test: pytest codestyle docstyle lint

cov-html:
	python3 -m pytest --cov=app_helpers --cov-report=term-missing --cov-report=html

cov-xml:
	python3 -m pytest --cov=app_helpers --cov-report=term-missing --cov-report=xml

